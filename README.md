# Neximo backend

This project was made for the [code challenge](https://docs.google.com/document/d/1aeyLwnaFDOpqKgWeIoYzK8hUcQNENb3qwODDZw47YaU/edit?usp=sharing).

## Running and building

Thiese projact could be executed from every platform, wich supports pjthon.

Before the projact starting you have to create `.env` config file. To do it, just copy `.env.example`. There you have to fill the application secret, wich could be got from random generator. 

To be able to use Instagram oAuth, you need to get Instagram secret and key, you have to follow the instractions on [Instagram dev portal](https://www.instagram.com/developer/). Also you need to add your service URL in client management -> Security -> Valid redirect URIs.

To run the projact you need to install python and python-pip, according to your operation system, then you need to perfom the following operations:

###### 
		pip install virtualenv 					# installs virtual environment support, it helps to make systemwide python clean and prevents dependences problems
		virtualenv -p PATH_TO_PYTHON env 		# sets up current folder to make it possible to use as python wrapper
		pip install -r ./requirements.txt 		# installs projact requements to local env
		python manage.py migrate				# creates sqlite3 database
		python manage.py createsuperuser		# creates superuser for administartion and maintance
		python manage.py runserver    			# finally starts the server
###### 
